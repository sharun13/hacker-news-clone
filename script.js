const topStories = 'https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty%27'

let myApp = angular.module('hackerNews', [])
    .controller('hackerNewsController', function ($scope, $http) {
        $scope.hackerNews = [];
        $scope.pageSize = 30;
        $scope.start = 0;
        $scope.nextPage = () => {
            if ($scope.start <= ($scope.hackerNews.length - 30)) {
                $scope.start += 30;
            }
        }
        $scope.previousPage = () => {
            if ($scope.start !== 0) {
                $scope.start -= 30;
            }
        }
        $scope.timeConverter = (timeStamp) => {
            let time = new Date(timeStamp * 1000)
            return time;
        }
        $http.get(topStories)
            .then((response) => {
                return response.data;
            })
            .then((data) => {
                data.map((news) => {
                    $http.get(`https://hacker-news.firebaseio.com/v0/item/${news}.json?print=pretty`)
                        .then((response) => {
                            $scope.hackerNews.push(response.data);
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                });
            })
            .catch((error) => {
                console.log(error);
            });
    })